def add(n, m)
  return n + m
end

def subtract(n, m)
  return n - m
end

def sum(numbers)
  numbers.empty? ? 0 : numbers.reduce(:+)
end

def multiply(numbers)
  numbers.reduce(:*)
end

def power(n, m)
  n**m
end

def factorial(n)
  return 1 if n == 0
  total = 1
  (1..n).step(1){|num| total *= num }
  total
end
