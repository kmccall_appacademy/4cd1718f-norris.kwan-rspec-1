def echo(command)
  command
end

def shout(command)
  command.upcase
end

def repeat(command, n = 2)
  ([command] * n).join(' ')
end

def start_of_word(command, n)
  command[0..n - 1]
end

def first_word(command)
  command.split.first
end

def titleize(command)
  ans = []
  words = command.split
  words.map { |word| "and over the".include?(word) ? ans << word : ans << word.capitalize }
  ans[0] = ans.first.capitalize
  ans.join(" ")
end

titleize('war and peace')
