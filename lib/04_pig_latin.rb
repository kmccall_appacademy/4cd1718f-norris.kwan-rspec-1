def translate(str)
  ans = []

  words = str.split
  words.each do |word|
    letters = word.chars
    if "aeiou".include?(letters.first)
      ans << word + "ay"
    elsif is_phoneme(word)
      phoneme = is_phoneme(word)
      phoneme_index = word.index(phoneme)
      cut_phoneme = word[0...phoneme_index] + phoneme
      ans << word.delete(cut_phoneme) + cut_phoneme + "ay"
    else
      word = word[1..-1] + word[0] until "aeiou".include?(word[0].downcase)
      ans << word + "ay"
    end
  end
  p ans.join(" ")
end

def is_phoneme(word)
  phoneme = ['sch', 'qu']
  phoneme.each do |phon|
    if word.include? phon
      return phon
    end
  end
  return false
end
